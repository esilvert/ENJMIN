* Tyran: 
** "jamais être assuré de la bonté, qui a toujours le pouvoir d'être méchant quand il le voudra."  
** Ne va jamais à la guerre (p3)
** "Plus les tyrans pillent, plus ils exigent" 
** Types: 
*** Par élection du peuple 
*** Par la force des armes 
*** Par succession de race

** "Les tyrans qui, faisant du mal à tous, sont obligés de craindre tout le monde".
** "Il en a toujours été ainsi: cinq ou six ont eu l'oreille du tyran et s'en sont approchés d'eux-mêmes".
*** "sous le grand tyran, autant de petits tyranneaux"
*** "Ils ne s'aiment pas mais se craignent. Ils ne sont pas amis, mais complices."


* Bon gouverneur:
** "qui aimait mieux avoir sauvé la vie d'un citoyen que d'avoir défait cent ennemis."


* Citoyens
"Les uns ont toujours devant les yeux le bonheur de leur vie passée et l'attente d'un vien-être égal pour l'avenir." En parlant de la guerre.

** Citoyen soumis
*** "Fasciné et pour ainsi dire ensocelés par le seul nom"
*** "Il ne sert pas de son gré, mais bien sous notre contrainte" -- Le cheval
*** "Ils prennent pour leur état de nature l'état de leur naissance."
*** "On ne regrette jamais cec qu'on n'a jamais-eu."
*** "Ainsi la première raison de la servitude volontaire, c'est l'habitude" /!\ Citation du titre /!\
*** "Les gens soumis n'ont ni ardeur ni pugnacité au combat"
*** "Il est soupçonneux envers celui qui l'aime et confiant envers celui qui le trompe."



** Citoyen libres
*** "L'amitié est un nom sacré, une chose sainte. Elle n'existe qu'entre gens de biens."
*** "Ils ne veulent pas amoindrir leur force en se désunissant"

* Morale
** "Il me semble, en effet, naturel d'avoir de la bonté pour celui qui nous a procuré du bien et de ne pas en craindre un mal."
** "Il ne s'agit pas de lui ôter quelque chose, mais de ne rien lui donner."
** "D'où tire-t-il tous ces yeux qui vous épient si ce n'est de vous ? Comment a-t-il tant de mains pour vous frapper, s'il ne vous les emprunte ? "
** En parlant de la nature : "nous sommes tous égaux, ou plutôt frères"
** Les animaux se battent pour leur liberté avant de se soumettre.
** "Ils ne faisaient que recouvrer une part de leur bien, et que cette part même qu'ils en recouvraient, le tyrant n'aurais pu la leur donner si, auparavant, il ne la leur avait enlevée."

* Gameplay
** "Il y établit des bordels, des tavernes et des jeux publics, et publia un ordonnance qui obligeait les citoyens à s'y rendre."
** "Le théâtre, les jeux, les farces, les spectacles, les gladiateurs, les bêtes curieuses, les médailles, les tableaux et autres drogues"
